require "spec_helper"

def create_book(title, date, pages, purchases)
  Book.create(title: title, published_on: date, pages_count: pages, purchases_count: purchases)
end

describe Book do
  before do
    @gatsby    = create_book("The Great Gatsby",       "1925-4-10", 180, 50_000)
    @eighty4   = create_book("1984",                   "1950-7-1",  328, 75_000)
    @catcher   = create_book("The Catcher in the Rye", "1951-7-16", 224, 33_000)
    @slaughter = create_book("Slaughterhouse-Five",    "1969-1-1",  215, 7_500)
    @mice      = create_book("Of Mice and Men",        "1937-1-1",  187, 66_500)
  end

  describe ".alphabetic" do
    it "returns all the books ordered alphabetically by title" do
      expect(Book.alphabetic).to eq [@eighty4, @mice, @slaughter, @catcher, @gatsby]
    end
  end

  describe ".top_sellers" do
    it "returns the top 3 books sorted by purchases count" do
      expect(Book.top_sellers).to eq [@eighty4, @mice, @gatsby]
    end
  end

  describe ".top_seller" do
    it "returns just the top selling book" do
      expect(Book.top_seller).to eq @eighty4
    end
  end

  describe ".longer_than" do
    it "returns the collection of books longer than the given page count" do
      expect(Book.longer_than(300)).to eq [@eighty4]
    end

    it "defaults to 0 pages when no argument is given" do
      expect(Book.longer_than.count).to eq 5
    end
  end

  describe ".published_between" do
    it "returns the collection of books published between the two given dates" do
      expect(Book.published_between("1951-1-1", "2000-1-1")).to eq [@catcher, @slaughter]
    end
  end
end
