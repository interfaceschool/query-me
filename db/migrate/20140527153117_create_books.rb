class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.string :title, null: false
      t.date :published_on
      t.integer :pages_count, default: 0
      t.integer :purchases_count, default: 0

      t.timestamps
    end
  end
end
