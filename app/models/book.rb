# == Schema Information
#
# Table name: books
#
#  id              :integer          not null, primary key
#  title           :string(255)      not null
#  published_on    :date
#  pages_count     :integer          default(0)
#  purchases_count :integer          default(0)
#  created_at      :datetime
#  updated_at      :datetime
#

class Book < ActiveRecord::Base
  validates :title, presence: true
end
